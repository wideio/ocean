import os
import subprocess
import sys

import mock
from ocean import get_ocean


def _system(cmd, manifest="manifest"):
    """Run tests in a modified environment."""
    env = os.environ.copy()
    env["OCEAN_MANIFEST"] = os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, env=env)
    _o, _e = p.communicate("")
    if p.returncode:
        sys.stdout.write(_o)
        sys.stderr.write(_e)
    return p.returncode


def test_ocean_ps():
    """Ocean has a command ps that takes no argument."""
    r = _system("ocean ps", manifest="empty")
    assert (r == 0)


def test_ocean_shell():
    """Ocean shell busybox must run busybox image."""
    r = _system("ocean shell busybox -c 'true'", manifest="busybox")
    assert (r == 0)


def test_get_section_is_memoized():
    manifest = "busybox"
    with mock.patch('os.environ') as mock_os_environ:
        mock_os_environ.get = mock.MagicMock(side_effect=lambda x, y=None: y)
        ocean = get_ocean(
            force=True,
            MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest + ".json")
        )
        x = ocean._get("busybox")
        y = ocean._get("busybox")
        assert x is y
