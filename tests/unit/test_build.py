# flake8: noqa
import functools
import os
import sys

import mock
import six
from ocean import get_ocean


def env_test(stdout=None, stderr=None):
    def dec(nt):
        def new_test():
            manifest = "environments.yaml"
            with mock.patch('os.environ') as mock_ocean_os_environment:
                # with mock.patch('ocean.os.environ') as mock_ocean_os_environment:
                with mock.patch('ocean.modules.mixin_build_step.subprocess') as mock_subprocess:
                    mock_ocean_os_environment.get = mock.MagicMock(side_effect=lambda x, y=None: y)
                    comres = mock.MagicMock()
                    comres.returncode = 0
                    comres.communicate.return_value = stdout, stderr
                    mock_subprocess.Popen.return_value = comres
                    ocean = get_ocean(True,
                                      MP=os.path.join(os.path.dirname(__file__), "fixtures", "manifest", manifest)
                                      )
                    nt(ocean)
        return functools.wraps(nt)(new_test)
    return dec


# ensures that the command are not faulty
@env_test("12e232ee".encode('utf8'))
def test_get_commit_id(*args):
    ocean = args[0]
    r = ocean.get_commitid('test-env')
    assert isinstance(r, six.string_types) and (sys.version[0] == "2" or not isinstance(r, bytes))
