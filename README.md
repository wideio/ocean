[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b3cd6b67af9745d3b4585c1a1250822c)](https://www.codacy.com/app/bn/ocean?utm_source=gitlab.pet.wide.io&amp;utm_medium=referral&amp;utm_content=wide-io-oss/ocean&amp;utm_campaign=Badge_Grade)

# OCEAN

OCEAN is a small extensible tool to allow to manage source, build environements and deployment environements
in an integrated in uniform way. The key idea being that developer must use similar tools as devops
team to be sure that software is deployed in good condition.

## License

`OCEAN` is released under a BSD-style license.

## Differences with docker-compose

- `docker-compose` focuses only on launching an app in a cluster.
  `ocean` manages the stream from the `git` repo to the application.
