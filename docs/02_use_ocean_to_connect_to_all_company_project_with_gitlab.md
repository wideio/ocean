## Using OCEAN with private gitlab for developer

It is possible for businesses for developpers to have a unified access to their gitlab.

To do this the developer must first start by configuring its `.python-gitlab` client.

```
cat << EoF > ~/.python-gitlab.cfg
[global]
default = mycorp
ssl_verify = true
timeout = 5

[mycorp]
url = https://gitlab.mycorp.com/
private_token = dfslfkdsflkDSFDSA
EoF
```

The next steps implies consists in defining where to store the manifest.

```
export OCEANPATH=$HOME/ocean

mkdir -p $OCEANPATH/manifest
ocean repo_manifest > $OCEANPATH/manifest/manifest.json
```

It shall then be explore and build specific project

```
ocean clone $PROJECT
ocean describe $PROJECT # development config must now be visible
ocean build $PROJECT
```

And finally to validate an run the project.

```
ocean build $PROJECT
ocean start $PROJECT
```

<!--
#ocean dev $PROJECT

ocean autoupdate_probe_once # probe once
```
-->