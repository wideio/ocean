## Defining your micro-services configuration with ocean

`ocean` uses a manifest to define the link between microservices.

A manifest is typically composed of numerous `section` and  `subsection`. Section correspond to projects
and subsections to the actual micro-services or code repositories related to this project.

Manifests can be written in `yaml` or in `json` and can be optionally encrypted and signed to ensure confidentiality and
authenticity of configuration information during deployments.

Hence a typical manifest stored for a developer will look like
```
project:
   redis-0:
      dockerimg: redis
   server-0:
      src: ssh://git.wide.io/demoserver
      requires:
         - redis-0
      browser: https://$CONTAINER_IP$:3000/
```

Once this is defined, it is possible to start the server simply by doing:

```
ocean start server-0
```

One of the key benefits in this case is that server-0 is not initially deployed as a
container and the latest source code will be fetched and build for ocean.
Also, the use won't have to know the URL of the server to be able to run it.

As the manifest is naturally split into project, it is also common to split the manifest into submanifests
that are attached to the project. This allow common deployment settings to be shared between developpers.
This is done by creating `submanifests`.
Creating a submanifest consists simply in listing all the services required by a project and in including
a variable inside of the project name so that the content of the manifest can be linked with current project.


```
$SRVNAME$-redis-0:
  dockerimg: redis
$SRVNAME$:
  src: ssh://git.wide.io/demoserver
  requires:
     - $SRVNAME$-redis-0
  browser: https://$CONTAINER_IP$:3000/
```

### Options can be used to configure micro-services
There are many other keywords that can be used in a manifest. Here is a
list of the options that are currently supported:

#### Runtime related options
- `alt-src` : Alternate path for source
- `config-files`: List of key-values
- `requires`: List of dependencies
- `command` : Command to be run when container starts

#### Build related options
- `dockerimg`: Reference to a source dockerimg
- `dockerfile`: Reference to a dockerfile specifying how to build the project
- `build-command`: command to be run to build the program from its sources

#### Tests related to webservice
- `bdd-command`: command to be run to bdd-tests
- `tdd-command`: command to be run to tdd-tests

#### Option related to webservice
- `nginx`: Section allowing to specify the NGINX configuration associated with that webserver.

#### Option related to DNS names of containers
- `cnames` : DNS names to be associated with the container
- `cname-target` : alternate target for the cname records

### Plugins

Ocean supports modules and plugins and new options may be added for additional features related to
the deployment and configuration of the services.
